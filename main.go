package main

import (
	"github.com/aws/aws-lambda-go/lambda"
	alexa "gitlab.com/dasjott/alexa-sdk-go"
	"gitlab.com/dasjott/skill-sample-fact/app"
)

func main() {
	alexa.AppID = ""
	alexa.Handlers = app.Handlers
	alexa.LocaleStrings = app.Locales
	lambda.Start(alexa.Handle)
}
