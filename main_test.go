package main_test

import (
	"encoding/json"
	"io/ioutil"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/dasjott/alexa-sdk-go"
	"gitlab.com/dasjott/alexa-sdk-go/dialog"
	"gitlab.com/dasjott/skill-sample-fact/app"
)

func init() {
	log.SetLevel(log.DebugLevel)

	alexa.AppID = "abc123"
	alexa.Handlers = app.Handlers
	alexa.LocaleStrings = app.Locales
}

func TestLaunch(t *testing.T) {
	test, resp := start(t, "launch")

	test.NotEmpty(resp.Response.OutputSpeech.SSML)
	t.Log(resp.Response.OutputSpeech.SSML)
}

func start(t *testing.T, name string) (*assert.Assertions, *dialog.EchoResponse) {
	test := assert.New(t)

	data, err := ioutil.ReadFile("testdata/" + name + ".json")
	if err != nil {
		panic(err)
	}

	req := dialog.EchoRequest{}
	if err := json.Unmarshal(data, &req); err != nil {
		panic(err)
	}

	resp, err := alexa.Handle(&req)
	test.Nil(err, name)
	test.NotNil(resp, name)

	return test, resp
}
