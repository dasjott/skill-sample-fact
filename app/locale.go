package app

import alexa "gitlab.com/dasjott/alexa-sdk-go"

// Locales are all the speeches
var Locales = alexa.Localisation{
	"en-US": alexa.Translation{
		"PAUSE": "<break time='300ms'/>",

		"GET_FACT_MESSAGE": "Here's your fact: ",
		"HELP_MESSAGE":     "You can say tell me a space fact, or, you can say exit... What can I help you with?",
		"HELP_REPROMPT":    "What can I help you with?",
		"SKILL_NAME":       "American Space Facts",
		"HELLO":            "Welcome to American Space Facts!",

		"STOP_MESSAGE": []string{
			"Bye.",
			"See you later.",
			"See you next time.",
		},

		"Fact": []string{
			"A year on Mercury is just 88 days long.",
			"Despite being farther from the Sun, Venus experiences higher temperatures than Mercury.",
			"Venus rotates counter-clockwise, possibly because of a collision in the past with an asteroid.",
			"On Mars, the Sun appears about half the size as it does on Earth.",
			"Earth is the only planet not named after a god.",
			"Jupiter has the shortest day of all the planets.",
			"The Milky Way galaxy will collide with the Andromeda Galaxy in about 5 billion years.",
			"The Sun contains 99.86% of the mass in the Solar System.",
			"The Sun is an almost perfect sphere.",
			"A total solar eclipse can happen once every 1 to 2 years. This makes them a rare event.",
			"Saturn radiates two and a half times more energy into space than it receives from the sun.",
			"The temperature inside the Sun can reach 15 million degrees Celsius.",
			"The Moon is moving approximately 3.8 cm away from our planet every year.",
		},
	},
	"de-DE": alexa.Translation{
		"PAUSE": "<break time='300ms'/>",

		"GET_FACT_MESSAGE": "Hier ist dein Fakt: ",
		"HELP_MESSAGE":     "Du kannst sagen erzähle mir einen Weltraum Fakt, oder du kannst beenden... Was kann ich für dich tun?",
		"HELP_REPROMPT":    "Was kann ich für dich tun?",
		"SKILL_NAME":       "Deutsche Weltraum Fakten",
		"HELLO":            "Herzlich Willkommen bei Deutsche Weltraum Fakten!",

		"STOP_MESSAGE": []string{
			"Tschüss.",
			"Bis später.",
			"Bis zum nächsten Mal.",
		},

		"Fact": []string{
			"Ein Jahr dauert auf dem Merkur nur 88 Tage.",
			"Die Venus ist zwar weiter von der Sonne entfernt, hat aber höhere Temperaturen als Merkur.",
			"Venus dreht sich entgegen dem Uhrzeigersinn, möglicherweise aufgrund eines früheren Zusammenstoßes mit einem Asteroiden.",
			"Auf dem Mars erscheint die Sonne nur halb so groß wie auf der Erde.",
			"Die Erde ist der einzige Planet, der nicht nach einem Gott benannt ist.",
			"Jupiter hat den kürzesten Tag aller Planeten.",
			"Die Milchstraßengalaxis wird in etwa 5 Milliarden Jahren mit der Andromeda-Galaxis zusammenstoßen.",
			"Die Sonne macht rund 99,86 % der Masse im Sonnensystem aus.",
			"Die Sonne ist eine fast perfekte Kugel.",
			"Eine Sonnenfinsternis kann alle ein bis zwei Jahre eintreten. Sie ist daher ein seltenes Ereignis.",
			"Der Saturn strahlt zweieinhalb mal mehr Energie in den Weltraum aus als er von der Sonne erhält.",
			"Die Temperatur in der Sonne kann 15 Millionen Grad Celsius erreichen.",
			"Der Mond entfernt sich von unserem Planeten etwa 3,8 cm pro Jahr.",
		},
	},
}

/*

var SKILL_NAME = "American Space Facts";
var GET_FACT_MESSAGE = "Here's your fact: ";
var HELP_MESSAGE = "You can say tell me a space fact, or, you can say exit... What can I help you with?";
var HELP_REPROMPT = "What can I help you with?";
var STOP_MESSAGE = "Goodbye!";
*/
