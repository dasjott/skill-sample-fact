package app

import alexa "gitlab.com/dasjott/alexa-sdk-go"

func launch(c *alexa.Context) {
	c.Ask(c.T("HELLO", "PAUSE", "HELP_REPROMPT"))
}

func getFact(c *alexa.Context) {
	fact := c.T("Fact")
	c.Ask(c.T("GET_FACT_MESSAGE")+fact+c.T("PAUSE", "HELP_REPROMPT")).SimpleCard(c.T("SKILL_NAME"), fact)
}

func help(c *alexa.Context) {
	c.Ask(c.T("HELP_MESSAGE"), c.T("HELP_REPROMPT"))
}

func bye(c *alexa.Context) {
	c.Tell(c.T("STOP_MESSAGE"))
}

// Handlers maps intent functions to intent names
var Handlers = alexa.IntentHandlers{
	"LaunchRequest":       launch,
	"GetNewFactIntent":    getFact,
	"AMAZON.HelpIntent":   help,
	"AMAZON.CancelIntent": bye,
	"AMAZON.StopIntent":   bye,
	"Unhandled":           bye,
}
