# skill sample fact

## how to start
Make sure you have created a lambda whose name is the PROJECT_NAME of the makefile (choose one)<br>
Make sure you have the aws cli configured and ready to be used.<br>
You also may want to write some more tests any time...<br>
<br>
In the directory do the following:<br>
```
go get ./...
make test
make publish
```

That's it.

- The first line gets all dependencies
- Second line executes the test(s)
- third line compiles and publishes the code to lambda


## Dependencies
GO of course.
AWS command line tool, configured with your access data. If you do not want to use the default profile, you may want to add `--profile yourName` to the line before the last, right behind `aws`.
ZIP comand line tool.

